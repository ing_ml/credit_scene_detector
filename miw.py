import cv2
import math
import numpy as np
import torch
import logging
from torchvision import transforms
from PIL import Image
import matplotlib.pyplot as plt

LOGGER = logging.getLogger(__name__)
AREAS = 3  # Number of areas to divide the frames into
STARTING_POINT = 0.96  # Starting point for frame processing
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class MovieInputWrapper:
    def __init__(self, video_path):
        self.video_path = video_path
        self.metadata = []  # Contains the timestamp (in milliseconds) and frame ID of all frames fed into the model
        self.capture = cv2.VideoCapture(self.video_path)
        self.width = self.capture.get(3)
        self.height = self.capture.get(4)
        self.cutoff = int((self.width - self.height) / (AREAS - 1))
        self.frame_rate = self.capture.get(5)
        self.total_frames = self.capture.get(7)
        self.credits_start_frame = None

        LOGGER.info(f"Movie metadata - width: {self.width}, height: {self.height}, framerate: {self.frame_rate}, "
                    f"total_frames: {self.total_frames}, currentframe: {self.capture.get(1)}")

    def process_frames(self, model):
        frame_set = []
        for i in range(AREAS):
            frames = []
            self.capture.set(1, int(self.total_frames * STARTING_POINT))
            while self.capture.isOpened():
                frame_info = {"time_progress": self.capture.get(0),
                              "frame_id": self.capture.get(1)}
                ret, frame = self.capture.read()
                if not ret:
                    break
                if frame_info['frame_id'] % math.floor(self.frame_rate / 10) == 0:
                    self.metadata.append(frame_info)
                    step = i * self.cutoff
                    frame = frame[:, step:int(self.height) + step, :]
                    # frame = cv2.resize(frame, (224, 224)) / 255.0
                    frames.append(frame)

                    # Check if it's the first frame where credits start rolling
                    if self.credits_start_frame is None:
                        with torch.no_grad():
                            image_pil = Image.fromarray(np.array(frame, dtype=np.uint8))
                            preprocess = transforms.Compose([
                                transforms.Resize((224, 224)),
                                transforms.ToTensor(),
                                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                            ])

                            input_tensor = preprocess(image_pil)
                            input_tensor = input_tensor.to(device)
                            input_batch = input_tensor.unsqueeze(0)
                            output = torch.sigmoid(model(input_batch)).item()

                            if output < 0.3:  # Adjust the threshold based on your model's confidence
                                def zero_pad(variable):
                                    return f'0{int(variable)}' if int(variable) < 10 else str(int(variable))
                                hour = frame_info['time_progress'] // 3600000
                                minute = (frame_info['time_progress'] - hour * 3600000) // 60000
                                second = (frame_info['time_progress'] - hour * 3600000 - minute * 60000) // 1000
                                hour, minute, second = zero_pad(hour), zero_pad(minute), zero_pad(second)
                                ms = int(math.floor(frame_info['time_progress']) % 1000)
                                self.credits_start_frame = {"id": frame_info['frame_id'], "time": f"{hour}:{minute}:{second}.{ms}"}
                                break

            frame_set.append(frames)

        LOGGER.info("Finished preprocessing frames.")
        self.capture.release()

        return frame_set, self.metadata,  self.credits_start_frame


